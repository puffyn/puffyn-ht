Currently this a proto-theme, not a ready-for-prime-time true theme

The theme itself is taking the [Accessible Minimalism](https://themes.gohugo.io/accessible-minimalism-hugo-theme/) theme as a starting point, with the exception that the `exampleSite` directory is instead taking the [hugoBasicExample](https://github.com/gohugoio/HugoBasicExample) repository as a starting point

Please check back later